﻿using System;
using System.Globalization;

namespace NextBiggerTask
{
    public static class NumberExtension
    {
        /// <summary>
        /// Finds the nearest largest integer consisting of the digits of the given positive integer number and null if no such number exists.
        /// </summary>
        /// <param name="number">Source number.</param>
        /// <returns>
        /// The nearest largest integer consisting of the digits  of the given positive integer and null if no such number exists.
        /// </returns>
        /// <exception cref="ArgumentException">Thrown when source number is less than 0.</exception>
        public static int? NextBiggerThan(int number)
        {
            if (number < 0)
            {
                throw new ArgumentException("number is less 0.");
            }

            if (number == int.MaxValue)
            {
                return null;
            }

            string stringNumber = number.ToString(CultureInfo.InvariantCulture);

            int numberLength = stringNumber.Length;

            int[] arrayNumber = new int[numberLength];
            for (int i = 0; i < numberLength; i++)
            {
                arrayNumber[i] = Convert.ToInt32(stringNumber[i]) - 48;
            }

            if (numberLength == 2)
            {
                if (arrayNumber[0] < arrayNumber[1])
                {
                    (arrayNumber[1], arrayNumber[0]) = (arrayNumber[0], arrayNumber[1]);
                    return Convert.ToInt32(string.Concat(arrayNumber).ToString(CultureInfo.InvariantCulture), CultureInfo.InvariantCulture);
                }

                return null;                
            }

            int index = -1;
            for (int i = numberLength - 1; i > 0; i--)
            {
                if (arrayNumber[i] > arrayNumber[i - 1])
                {
                    (arrayNumber[i], arrayNumber[i - 1]) = (arrayNumber[i - 1], arrayNumber[i]);
                    index = i;
                    break;
                }
            }

            if (index < 0)
            {
                return null;
            }

            if (numberLength - index < 2)
            {
                return Convert.ToInt32(string.Concat(arrayNumber).ToString(CultureInfo.InvariantCulture), CultureInfo.InvariantCulture);
            }

            int[] temp = new int[numberLength - index];

            Array.ConstrainedCopy(arrayNumber, index, temp, 0, temp.Length);

            Array.Sort(temp);

            for (int i = 0; i < temp.Length; i++)
            {
                arrayNumber[index + i] = temp[i];
            }

            return Convert.ToInt32(string.Concat(arrayNumber).ToString(CultureInfo.InvariantCulture), CultureInfo.InvariantCulture);
        }
    }
}
